from _sha1 import sha1
from unittest import TestCase
from datetime import datetime

from pony.orm import select, db_session, count, delete

from desafio_concrete.models import User
from desafio_concrete.querysets import (
    add_user, check_if_email_exists, login_user, update_user_last_login,
    update_user_last_activity
)


class TestQuerySet(TestCase):

    @db_session
    def setUp(self):
        delete(user for user in User)

    @db_session
    def test_add_user(self):
        user = {
            'name': 'user_name',
            'email': 'user_mail',
            'password': 'user_pwd',
        }
        phones = [
            {
                'ddd': 21,
                'number': 123
            },
            {
                'ddd': 21,
                'number': 123
            }
        ]
        add_user(user=user, phones=phones)

        persisted_user = select(user for user in User)[:][0]
        persisted_phones = list(persisted_user.phones)
        user_sha1 = sha1('user_pwd'.encode('utf-8')).hexdigest()

        self.assertEqual(persisted_user.name, 'user_name')
        self.assertEqual(persisted_user.email, 'user_mail')
        self.assertEqual(persisted_user.password, user_sha1)
        self.assertIsInstance(persisted_user.created, datetime)
        self.assertIsNotNone(persisted_user.token)
        self.assertEqual(count(persisted_user.phones), 2)
        self.assertEqual(persisted_phones[0].ddd, 21)
        self.assertEqual(persisted_phones[0].number, 123)
        self.assertEqual(persisted_phones[1].ddd, 21)
        self.assertEqual(persisted_phones[1].number, 123)

    @db_session
    def test_add_user_with_invalid_name_field(self):
        user = {
            'name_': 'user_name',
            'email': 'user_mail',
            'password': 'user_pwd',
        }
        self.assertRaises(KeyError, add_user, user=user)

    @db_session
    def test_add_user_with_invalid_value(self):
        user = {
            'name': [],
            'email': 'silva@gmail.com',
            'password': '123321'
        }
        self.assertRaises(TypeError, add_user, user=user)

    @db_session
    def test_add_test_return_user_id(self):
        user = {
            'name': 'user_name',
            'email': 'user_mail',
            'password': 'user_pwd'
        }
        persisted_user = add_user(user=user)
        self.assertEqual(count(user for user in User), 1)
        self.assertEqual(persisted_user.name, 'user_name')

    @db_session
    def test_email_exists(self):
        user = {
            'name': 'user_name',
            'email': 'user_mail',
            'password': 'user_pwd'
        }
        add_user(user=user)
        self.assertTrue(check_if_email_exists('user_mail'))

    @db_session
    def test_email_does_not_exists(self):
        user = {
            'name': 'user_name',
            'email': 'user_mail',
            'password': 'user_pwd'
        }
        add_user(user=user)
        self.assertFalse(check_if_email_exists('email'))

    @db_session
    def test_login(self):
        user = {
            'name': 'user_name',
            'email': 'user_mail',
            'password': 'user_pwd'
        }
        add_user(user=user)
        self.assertTrue(login_user('user_mail', 'user_pwd'))

    @db_session
    def test_login_with_invalid_email(self):
        user = {
            'name': 'user_name',
            'email': 'user_mail',
            'password': 'user_pwd'
        }
        add_user(user=user)
        self.assertFalse(login_user('_mail', 'user_pwd'))

    @db_session
    def test_login_with_invalid_password(self):
        user = {
            'name': 'user_name',
            'email': 'user_mail',
            'password': 'user_pwd'
        }
        add_user(user=user)
        self.assertFalse(login_user('user_mail', '_pwd'))

    @db_session
    def test_update_user_last_login(self):
        user = {
            'name': 'user_name',
            'email': 'user_mail',
            'password': 'user_pwd'
        }
        db_user = add_user(user=user)
        self.assertIsNone(db_user.last_login)
        self.assertIsNone(db_user.last_activity)
        db_user = update_user_last_login(db_user)
        self.assertIsInstance(db_user.last_login, datetime)
        self.assertIsInstance(db_user.last_activity, datetime)

    @db_session
    def test_update_user_last_login(self):
        user = {
            'name': 'user_name',
            'email': 'user_mail',
            'password': 'user_pwd'
        }
        db_user = add_user(user=user)
        self.assertIsNone(db_user.last_activity)
        db_user = update_user_last_activity(db_user)
        self.assertIsInstance(db_user.last_activity, datetime)

