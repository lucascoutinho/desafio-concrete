from unittest import TestCase

from webtest import TestApp

from desafio_concrete.__main__ import app
from desafio_concrete.models import create_tables

create_tables()


class BaseTest(TestCase):

    def __init__(self, *args, **kwargs):
        super(BaseTest, self).__init__(*args, **kwargs)
        self.app = TestApp(app)
