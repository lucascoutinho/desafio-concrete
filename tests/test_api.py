import json
import datetime

from pony.orm import db_session, delete, get

from desafio_concrete.models import User
from desafio_concrete.querysets import check_if_email_exists, add_user
from tests import BaseTest


class TestFunctionalApi(BaseTest):

    @db_session
    def setUp(self):
        self.user = {'name': 'João Silva', 'email': 'joao@silva.org.br',
                     'password': 'hunter2', 'phones': [{'number': 9010,
                                                        'ddd': 21},
                                                        {'number': 9011,
                                                         'ddd': 21}]}
        delete(user for user in User)

    @db_session
    def test_register_with_valid_input(self):
        response = self.app.post('/api/register',
                                 content_type='application/json', xhr=True,
                                 params=json.dumps(self.user))
        json_response = response.json
        user = get(user for user in User if user.id == json_response['id'])
        self.assertEqual('200 OK', response.status)
        self.assertEqual(user.name, 'João Silva')
        self.assertEqual(user.email, 'joao@silva.org.br')
        self.assertTrue(check_if_email_exists('joao@silva.org.br'))

    def test_register_with_invalid_input(self):
        params = {'name': 'João da Silva', 'email': 'joao@silva.org'}
        response = self.app.post('/api/register',
                                 content_type='application/json', xhr=True,
                                 params=json.dumps(params), expect_errors=True)

        self.assertEqual('400 Bad Request', response.status)
        self.assertEqual(response.json, {'mensagem':
                                         'There are keys missing in payload'})

    @db_session
    def test_register_with_email_already_used(self):
        add_user(self.user.copy())
        response = self.app.post('/api/register',
                                 content_type='application/json', xhr=True,
                                 params=json.dumps(self.user),
                                 expect_errors=True)

        self.assertEqual('409 Conflict', response.status)
        self.assertEqual(response.json, {'mensagem':
                                         'This email is already in use'})

    def test_register_with_type_error(self):
        params = {'name': 'João da Silva', 'email': 'teste@gmail.com',
                  'password': []}
        response = self.app.post('/api/register',
                                 content_type='application/json', xhr=True,
                                 params=json.dumps(params), expect_errors=True)

        self.assertEqual('400 Bad Request', response.status)
        self.assertEqual(response.json, {'mensagem': 'Bad request'})

    def test_login_unauthorized(self):
        params = {'email': 'lucas@silva.org', 'password': 'hunter2'}
        response = self.app.get('/api/login', headers={'Content-Type':
                                                       'application/json'},
                                xhr=True, params=params, expect_errors=True)

        self.assertEqual('401 Unauthorized', response.status)
        self.assertEqual(response.json, {'mensagem':
                                         'User and/or password invalids'})
        self.assertEqual(response.content_type, 'application/json')

    @db_session
    def test_login(self):
        user = {'name': 'João Silva', 'email': 'joao@silva.org.br',
                'password': 'hunter2', 'phones': [{'number': 9010,
                                                     'ddd': 21},
                                                    {'number': 9011,
                                                     'ddd': 21}]}
        add_user(user)
        params = {'email': 'joao@silva.org.br', 'password': 'hunter2'}
        response = self.app.get('/api/login', headers={'Content-Type':
                                                           'application/json'},
                                xhr=True, params=params, expect_errors=True)

        json_response = response.json
        user = get(user for user in User if user.id == json_response['id'])
        self.assertEqual('200 OK', response.status)
        self.assertEqual(user.name, 'João Silva')
        self.assertEqual(user.email, 'joao@silva.org.br')

    @db_session
    def test_get_profile_with_valid_token(self):
        user = {'name': 'João Silva', 'email': 'joao@silva.org.br',
                'password': 'hunter2', 'phones': [{'number': 9010,
                                                   'ddd': 21},
                                                  {'number': 9011,
                                                   'ddd': 21}]}
        db_user = add_user(user)
        self.assertIsNone(db_user.last_activity)
        response = self.app.get('/api/profile', headers={'Authentication':
                                                         db_user.token},
                                expect_errors=True)
        json_response = response.json
        user = get(user for user in User if user.id == json_response['id'])
        self.assertEqual('200 OK', response.status)
        self.assertEqual(user.name, 'João Silva')
        self.assertEqual(user.email, 'joao@silva.org.br')
        self.assertIsInstance(user.last_activity, datetime.datetime)

    @db_session
    def test_get_profile_with_invalid_token(self):
        user = {'name': 'João Silva', 'email': 'joao@silva.org.br',
                'password': 'hunter2', 'phones': [{'number': 9010,
                                                   'ddd': 21},
                                                  {'number': 9011,
                                                   'ddd': 21}]}
        add_user(user)
        response = self.app.get('/api/profile', headers={'Authentication':
                                                         'invalid_token'},
                                expect_errors=True)
        json_response = response.json
        self.assertEqual('401 Unauthorized', response.status)
        self.assertEqual(json_response, {'mensagem': 'Not authorized'})
