from http import HTTPStatus
from unittest import TestCase, mock

from datetime import datetime, timedelta

from pony.orm import db_session

from desafio_concrete.models import User
from desafio_concrete.querysets import add_user
from desafio_concrete.base_api import check_json_request, requires, verify_token


class TestBaseApi(TestCase):

    @mock.patch('desafio_concrete.base_api.response')
    @mock.patch('desafio_concrete.base_api.request')
    def test_is_json_request(self, mock_request, mock_response):
        mock_request.json = None

        @check_json_request
        def test_wrapper():
            return

        self.assertEqual(test_wrapper(), {'mensagem':
                                          'The request must be JSON'})
        self.assertEqual(mock_response.status, HTTPStatus.BAD_REQUEST)
        self.assertEqual(mock_response.content_type, 'application/json')

    @mock.patch('desafio_concrete.base_api.request')
    def test_requires_valid_fields(self, mock_request):

        mock_request.json = {'name': 'test_name'}

        @requires(required={'name'})
        def test_wrapper():
            return 'require is valid'

        self.assertEqual('require is valid', test_wrapper())

    @mock.patch('desafio_concrete.base_api.response')
    @mock.patch('desafio_concrete.base_api.request')
    def test_requires_invalid_fields(self, mock_request, mock_response):

        @requires(required={'name', 'email'})
        def test_wrapper():
            return 'require is valid'

        mock_request.json = {'name': 'test_name'}

        self.assertEqual(test_wrapper(), {'mensagem':
                                          'There are keys missing in payload'})
        self.assertEqual(mock_response.status, HTTPStatus.BAD_REQUEST)

    def test_requires_with_invalid_required_object(self):

        @requires(required=('name',))
        def test_wrapper():
            return 'require is valid'

        self.assertRaises(ValueError, test_wrapper)

    @db_session
    @mock.patch('desafio_concrete.base_api.response')
    @mock.patch('desafio_concrete.base_api.request')
    def test_verify_token(self, mock_request, mock_response):
        
        user = add_user({'name': 'token_test', 'email': 'fake@mail.com',
                         'password': '123'})
        user.last_activity = datetime.now()
        
        @verify_token
        def test_wrapper(user: User):
            return 'ok'

        mock_request.headers = {
            'Authentication': user.token
        }
        self.assertEqual(test_wrapper(), 'ok')
        self.assertEqual(mock_response.status, HTTPStatus.OK)

    @mock.patch('desafio_concrete.base_api.response')
    @mock.patch('desafio_concrete.base_api.request')
    def test_verify_token_not_authorized(self, mock_request, mock_response):

        @verify_token
        def test_wrapper(user: User):
            return 'ok'
        mock_request.headers = {
            'Authentication': 'token'
        }

        self.assertEqual(test_wrapper(), {'mensagem': 'Not authorized'})
        self.assertEqual(mock_response.status, HTTPStatus.UNAUTHORIZED)

    @db_session
    @mock.patch('desafio_concrete.base_api.response')
    @mock.patch('desafio_concrete.base_api.request')
    def test_verify_token_timeout(self, mock_request, mock_response):
        user = add_user({'name': 'timeout', 'email': 'fakemail@mail.com',
                         'password': '123'})
        user.last_activity = datetime.today() - timedelta(minutes=31)

        @verify_token
        def test_wrapper(user: User):
            return 'ok'

        mock_request.headers = {
            'Authentication': user.token
        }
        self.assertEqual(test_wrapper(), {'mensagem': 'Invalid session'})
        self.assertEqual(mock_response.status, HTTPStatus.PRECONDITION_FAILED)

