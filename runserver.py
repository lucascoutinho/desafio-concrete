#!/usr/bin/env python
"""
    Start up application server
"""
from argparse import ArgumentParser

from bottle import run, default_app

from desafio_concrete.consts import DEBUG, RELOAD_APP


def parse_args():
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('-p', '--port', type=int, default=8080,
                        help='default: 8080')
    parser.add_argument('-a', '--address', default='localhost',
                        help='default: localhost')
    parser.add_argument('-d', '--debug', default=False)
    parser.add_argument('-r', '--reload_app', default=False)
    return parser.parse_args()


def main():
    args = parse_args()
    run(host=args.address, port=args.port, debug=args.debug or DEBUG,
        reloader=args.reload_app or RELOAD_APP)

if __name__ == '__main__':
    main()
else:
    app = application = default_app()
