from pony.orm.core import Database

__all__ = ('db', )

db = Database()
db.bind('sqlite', ':memory:')


