from bottle import run, default_app

from desafio_concrete.consts import DEBUG, RELOAD_APP

if __name__ == '__main__':
    run(host='localhost', port=8080, debug=DEBUG, reloader=RELOAD_APP)
else:
    app = application = default_app()
