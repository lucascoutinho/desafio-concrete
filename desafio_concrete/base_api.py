from typing import Set
from functools import wraps
from http import HTTPStatus
from datetime import datetime, timedelta

from bottle import request, response
from pony.orm import db_session, get

from desafio_concrete.models import User


def error_message(msg):
    return {
        'mensagem': msg
    }


def check_json_request(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        response.content_type = 'application/json'
        if request.json:
            return func(*args, *kwargs)
        else:
            response.status = HTTPStatus.BAD_REQUEST
            return error_message(msg='The request must be JSON')
    return wrapper


def requires(required: Set, data_from='json'):
    def body_req_middleware(func):
        def inner(*args, **kwargs):
            if not isinstance(required, Set):
                raise ValueError('Required must be a set object')
            data = getattr(request, data_from, {})
            intersection = required.intersection(set(data.keys()))
            if intersection != required:
                response.status = HTTPStatus.BAD_REQUEST
                return error_message(msg='There are keys missing in payload')
            return func(*args, **kwargs)
        return inner
    return body_req_middleware


def verify_token(func):

    def unauthorized():
        response.status = HTTPStatus.UNAUTHORIZED
        return error_message(msg='Not authorized')

    @db_session
    @wraps(func)
    def wrapper(*args, **kwargs):
        token = request.headers.get('Authentication')
        if not token:
            return unauthorized()

        user = get(user for user in User if user.token == token.strip())
        if not user:
            return unauthorized()

        timeout = datetime.today() - timedelta(minutes=30)
        if user.last_activity is not None and user.last_activity < timeout:
            response.status = HTTPStatus.PRECONDITION_FAILED
            return error_message(msg='Invalid session')
        response.status = HTTPStatus.OK
        return func(user, *args, **kwargs)
    return wrapper


