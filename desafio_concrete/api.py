from http import HTTPStatus

from bottle import post, get, request, response
from pony.orm import db_session

from desafio_concrete.base_api import (
    check_json_request, requires, error_message, verify_token
)
from desafio_concrete.models import User
from desafio_concrete.querysets import (
    check_if_email_exists, add_user, login_user, update_user_last_login
)
from desafio_concrete.querysets import update_user_last_activity


__all__ = ('register', 'login')

REGISTER_REQUIRES = {'name', 'email', 'password'}


@post('/api/register', apply=[check_json_request, requires(REGISTER_REQUIRES)])
def register():
    if check_if_email_exists(request.json['email']):
        response.status = HTTPStatus.CONFLICT
        return error_message(msg='This email is already in use')
    try:
        with db_session:
            return add_user(request.json).to_dict()
    except (ValueError, TypeError, KeyError):
        response.status = HTTPStatus.BAD_REQUEST
        return error_message(msg='Bad request')


@get('/api/login')
def login():
    if request.query.email and request.query.password:
        user = login_user(email=request.query.email,
                          password=request.query.password)
        if not user:
            response.status = HTTPStatus.UNAUTHORIZED
            return error_message(msg='User and/or password invalids')
        return update_user_last_login(user).to_dict()
    else:
        response.status = HTTPStatus.BAD_REQUEST
        return error_message(msg='There are keys missing in payload')


@get('/api/profile', apply=[verify_token])
def profile(user: User):
    update_user_last_activity(user)
    return user.to_dict()
