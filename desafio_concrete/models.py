from datetime import datetime

from pony.orm import (
    Required, Optional, Set, composite_key
)

from desafio_concrete.config import db


class User(db.Entity):

    name = Required(str)
    email = Required(str)
    password = Required(str)
    last_login = Optional(datetime)
    last_activity = Optional(datetime)
    token = Required(str)
    phones = Set(lambda: Phone, cascade_delete=True)

    created = Required(datetime)
    updated = Optional(datetime, default=datetime.now)

    composite_key(name, email)

    def to_dict(self):
        return {
            'id': self.id,
            'created': str(self.created),
            'modified': str(self.updated),
            'last_login': str(self.last_login),
            'token': self.token
        }


class Phone(db.Entity):

    number = Required(int)
    ddd = Required(int)
    user = Optional(User)


def create_tables():
    db.generate_mapping(create_tables=True, check_tables=False)

