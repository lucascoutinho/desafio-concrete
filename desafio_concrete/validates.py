from email.utils import parseaddr

from desafio_concrete.models import User


def validate_user(user: User):
    str_fields = ['name', 'email', 'password']
    if not all(isinstance(user[attr], str) for attr in str_fields):
        raise TypeError('Invalid type for attribute')
    mail_name, email_addr = parseaddr(user['email'])
    if not email_addr:
        raise ValueError('Invalid email')
