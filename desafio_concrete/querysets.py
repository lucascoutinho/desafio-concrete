from datetime import datetime
from typing import Iterable, Dict
from uuid import uuid4
from hashlib import sha1

from pony.orm import db_session, commit, exists, get

from desafio_concrete.models import User, Phone
from desafio_concrete.validates import validate_user


@db_session
def add_user(user: Dict, phones: Iterable[Dict]=[]) -> User:
    validate_user(user)
    user['token'] = str(uuid4())
    user['created'] = datetime.now()
    user['password'] = sha1(user['password'].encode('utf-8')).hexdigest()
    db_user = User(** {** user, ** {'phones': [Phone(** phone) for phone
                                               in phones]}})
    commit()
    return db_user


@db_session
def update_user_last_login(user: User) -> User:
    user.last_login = datetime.now()
    user.last_activity = datetime.now()
    return user


@db_session
def update_user_last_activity(user: User) -> User:
    user.last_activity = datetime.now()
    return user


@db_session
def check_if_email_exists(email: str) -> bool:
    return exists(user for user in User if user.email == email.lower())


@db_session
def login_user(email: str, password: str) -> bool:
    password = sha1(password.encode('utf-8')).hexdigest()
    user = get(user for user in User if user.email == email.lower()
               and user.password == password)
    if user is not None:
        return user
    return False
