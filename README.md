# Desafio Concrete

__Crie uma aplicação que exponha uma API RESTful de criação de usuários e login.__

## Requisitos

* python 3.5
* linux

## Instalação

```
pip install -r requirements.txt
```

_Modo desenvolvedor_

```
pip install -r requirements-dev.txt
```

## Testes

```
python -m unittest -v
```


## Execução

```
./runserver.py
```

Para mais informações de configurações execute:

```
./runserver.py --help
```
